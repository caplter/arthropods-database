## arthropods database

*database migration: mysql::lter10_arthropods_production and mysql::mcdowell_arthropods to postgresql::arthropods*

### overview

The workflow detailed in this repository is to migrate two CAP LTER MySQL databases that house data related to the long-term monitoring of arthropods to a single Postgresql database. The CAP LTER maintains two long-term monitoring efforts that focus on regular, repeated collections of arthropods via pitfall trapping. One effort is a long-running program [link](https://portal.edirepository.org/nis/mapbrowse?scope=knb-lter-cap&identifier=41) started in the early days of the project with locations in different habitats throughout the greater Phoenix, Arizona metropolitan area and surrounding Sonoran desert. This effort is generally referred to as `core` or `lter10` arthropods. Another project [link](https://portal.edirepository.org/nis/mapbrowse?scope=knb-lter-cap&identifier=643) that employs identical methodology was developed years later in cooperation with the McDowell Sonoran Conservancy to monitor arthropods in the McDowell Sonoran Preserve of Scottsdale, Arizona. Data for each project was stored in a separate MySQL database with nearly identical structure, except that the taxonomic data were stored only in the McDowell database with that information being made accessible to the lter10 database via a view. In addition to migrating from MySQL to Postgresql, an important aspect of this update and migration was to merge the data from these separate databases into a single database. The schema for the new Postgresql database generally reflects the schema of the two MySQL databases (but see section below on arthropod taxonomy) with considerable data cleaning and standardization addressed during the process.

### arthropod taxonomy

Concomitant with the database(s) merge and migration was to address the long-standing, poorly implemented taxonomy. The arthropod taxonomy was built over the many years of the project but with an inefficient structure that, among other issues, conflated taxonomy with organism characteristics. For example, unique records would be added for different life stages (e.g., immature, winged) of an organism. In the new taxonomy, each record reflects a unique taxon with life-stage characteristics stored as flags associated with the observation rather than the taxon. Another problem and challenge was that the taxonomy reflected only the knowledge and familiarity of the observer at the time of observation. That is, the taxa were not linked to a taxonomic resource and were never updated to reflect taxonomic revisions. This problem was compounded as it was reflected across taxonomic resolutions where, for example, the Order, Family, and Genus of a taxon identified to the level of species could all (or selectively) be out of date or sync. The CAP LTER Information Manager and Arthropod project manager revised the taxonomy such that it reflected an up-to-date nomenclature where each taxon with only a few exceptions is resolvable to a taxonomic authority (typically [ITIS](https://itis.gov/) or [GBIF](https://www.gbif.org/)). Going forward, only taxa resolvable against a taxonomic authority will be added to the taxonomy.

### database schemas

#### lter10 and McDowell arthropods MySQL schemas

lter10 schema             |  McDowell schema
:-------------------------:|:-------------------------:
![lter10](assets/figures/lter10_arthropods_production_schema.png)  |  ![mcdowell](assets/figures/mcdowell_arthropods_schema.png)


#### Postgresql arthropods schema

![arthropods](assets/figures/arthropods_schema.png)


### quality control

Addressing inconsistent and inconsistently applied quality control flags is an additional aspect of the migration. New quality-control flags were attached at the level of sampling events, and a combination of new, revised, and more consistently applied flags were attached to trap sampling events.

#### sampling events

Data investigation revealed that there are some sampling events for which there are not any organisms recorded. The reason for this is not always known but by aligning those sampling events with corresponding trap sampling event comments, we can identify with a high degree of certainty a subset of those for which the samples were not set and/or collected (e.g., if the field of an agricultural site was plowed). These events are nothing more than a record of conditions on the ground, and do not reflect actual sampling effort (i.e., as opposed to a single trap that was not collected). We identify those sampling events for which not any organisms are reported. Where a corresponding trap sampling event comment indicates that the site was not sampled, we add a flag to the sampling event so that these data will not be included in data queries. For those sampling events where organisms were not present but a trap sampling event comment does not indicate why and/or is ambiguous, we add a flag to those trap sampling events (not sampling event!) that the sampling event was empty.

#### trap sampling events

For trap sampling events, the level at which quality-control flags were applied in the source schemas, we standardize existing quality-control flags, implement new flags as appropriate, and apply new or updated flags logically to trap sampling events. This work is based on an analysis of the data, notably whether organisms are reported for a given trap sampling event, and, technician notes added to the comments field. To be absolutely clear, this is improving the application of the quality control flags as much as possible given the information available - the information is incomplete and there are pronounced ambiguities from previous inconsistencies. 

Despite the potential for errors, it was felt that this work was important to improve the identity of records where traps were not collected/set and/or organisms were not detected. The distinction between these two conditions is extremely important because they each have profoundly different effects on sampling effort. At the same time, this distinction was also the most difficult to disambiguate. Notably, the comment of 'no arthropods' and similar variants. In this analysis, this text in the comment field was taken to mean that the trap was set and collected but that there were not organisms present in the collected trap. In this case, there is sampling effort. However, these assessments are based mostly on other comments that more clearly indicate when a trap was not set or collected (e.g., 'not collected', 'trap lost') and we cannot know with certainty that variants of 'no arthropods' absolutely and in all cases indicates that a trap was collected but was empty.
