#' @title create table of observers
#'
#' @description create table of observers
#'
#' @note Exploratory code (and output) to identify duplicate observers between
#' (and within) lter10 and msp people is featured at the top of this script.
#' The corrective actions to address duplicates in the mysql_people resource
#' are included in this script but the steps to address these discrepencies
#' among the data are included in the `merge_lter10_msp.R` workflow.

# this explatory code to identify duplicates will no longer work as the
# replicate column was removed

# observers[observers$replicate == 2, ]
# A tibble: 9 × 4
# Groups:   observer_name [9]
#    id observer_name   replicate active
# <int> <chr>               <int> <lgl> 
#    86 Chad Allen              2 FALSE 
#    87 Roxcelli Ortega         2 FALSE 
#    88 Maggie Tseng            2 FALSE 
#    89 Annie Deutsch           2 FALSE 
#    90 Andy Olson              2 FALSE 
#    91 David Fleming           2 FALSE 
#    92 Taylor Johnson          2 FALSE 
#    93 Gabriella Rich          2 FALSE 
#    94 Trinity Johnson         2 FALSE 

# mysql_people |>
# dplyr::filter(observer_name %in% c(observers[observers$replicate == 2, ][["observer_name"]])) |>
# dplyr::arrange(observer_name) |>
# dplyr::select( id, observer_name, tidyselect::contains("person"))

# id   observer_name lter10_person_id msp_person_id
# 80      Andy Olson               82            NA
# 90      Andy Olson               NA             6

# 79   Annie Deutsch               81            NA
# 89   Annie Deutsch               NA             5

# 76      Chad Allen               78            NA
# 86      Chad Allen               NA             2

# 81   David Fleming               83            NA
# 91   David Fleming               NA             7

# 83  Gabriella Rich               85            NA
# 93  Gabriella Rich               NA             9

# 59    Maggie Tseng               60            NA
# 88    Maggie Tseng               NA             4

# 78 Roxcelli Ortega               80            NA
# 87 Roxcelli Ortega               NA             3

# 82  Taylor Johnson               84            NA
# 92  Taylor Johnson               NA             8

# 84 Trinity Johnson               86            NA
# 94 Trinity Johnson               NA            10 # listed but no records

# address duplicates (observer in both lter10 & msp)
# mysql_trap_specimens[mysql_trap_specimens$person_id == 90, ][["person_id"]] <- 80
# mysql_trap_specimens[mysql_trap_specimens$person_id == 89, ][["person_id"]] <- 79
# mysql_trap_specimens[mysql_trap_specimens$person_id == 86, ][["person_id"]] <- 76
# mysql_trap_specimens[mysql_trap_specimens$person_id == 91, ][["person_id"]] <- 81
# mysql_trap_specimens[mysql_trap_specimens$person_id == 93, ][["person_id"]] <- 83
# mysql_trap_specimens[mysql_trap_specimens$person_id == 88, ][["person_id"]] <- 59
# mysql_trap_specimens[mysql_trap_specimens$person_id == 87, ][["person_id"]] <- 78
# mysql_trap_specimens[mysql_trap_specimens$person_id == 92, ][["person_id"]] <- 82

# merge duplicate maggie's
# mysql_trap_specimens[mysql_trap_specimens$person_id == 54, ][["person_id"]] <- 59

mysql_people <- mysql_people[-c(90, 89, 86, 91, 93, 88, 87, 92, 94), ]  # remove duplicates
mysql_people <- mysql_people[-c(85), ]                                  # remove ryan test
mysql_people <- mysql_people[-c(54), ]                                  # remove duplicate maggie

observers <- mysql_people |>
dplyr::mutate(
  across(where(is.character), ~ stringr::str_trim(., side = c("both"))),
  observer_name = dplyr::case_when(
    is.na(observer_name) ~ "unknown",
    TRUE ~ observer_name
  )
  ) |>
dplyr::mutate(
  active = NA,
  active = FALSE,
  active = dplyr::case_when(
    grepl("fleming", observer_name, ignore.case = TRUE) ~ TRUE,
    TRUE ~ active
    ),
  ) |>
dplyr::select(
  id,
  observer_name,
  active
)


# construct table and sequence
databaseDevelopmentTools::r_pg_table(
  schema = "arthropods",
  table  = "observers",
  owner  = this_configuration$owner
)

# add timestamping to create and update
databaseDevelopmentTools::add_timestamping(
  schema = "arthropods",
  table  = "observers"
)

# modify column types
dbExecute(pg, "
  ALTER TABLE arthropods.observers
    ALTER COLUMN observer_name SET NOT NULL ;
  ")

# add unique index
dbExecute(pg, "CREATE UNIQUE INDEX observers_observer_name ON arthropods.observers (observer_name) ;")
dbExecute(pg, "ALTER TABLE arthropods.observers ADD CONSTRAINT unique_observers_observer_name UNIQUE USING INDEX observers_observer_name ;")

# NOTICE:  ALTER TABLE / ADD CONSTRAINT USING INDEX will rename index "observers_observer_name" to "unique_observers_observer_name"
